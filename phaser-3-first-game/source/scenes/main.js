import Phaser from "../vendor/phaser.js";

var main = new Phaser.Scene( "main" );

function createPlayer( scene ){
	scene.player = scene.physics.add.sprite( 100, 450, "dude" );

	scene.player.setBounce( 0.2 );
	scene.player.setCollideWorldBounds( true );
}

function createAnimations( scene ){
	scene.anims.create( {
		"key": "left",
		"frames": scene.anims.generateFrameNumbers(
			"dude",
			{
				"start": 0,
				"end": 3
			}
		),
		"frameRate": 10,
		"repeat": -1
	} );

	scene.anims.create( {
		"key": "turn",
		"frames": [
			{
				"key": "dude",
				"frame": 4
			}
		],
		"frameRate": 20
	} );

	scene.anims.create( {
		"key": "right",
		"frames": scene.anims.generateFrameNumbers(
			"dude",
			{
				"start": 5,
				"end": 8
			}
		),
		"frameRate": 10,
		"repeat": -1
	} );
}

function createMap( scene ){
	scene.add.image( 400, 300, "sky" );
    
	createPlatforms( scene );
}

function createPlatforms( scene ){
	scene.platforms = scene.physics.add.staticGroup();

	scene.platforms
		.create( 400, 568, "ground" )
		.setScale( 2 )
		.refreshBody();

	scene.platforms.create( 600, 400, "ground" );
	scene.platforms.create( 50, 250, "ground" );
	scene.platforms.create( 750, 220, "ground" );
}

function addCollisions( scene ){
	scene.physics.add.collider( scene.player, scene.platforms );
	scene.physics.add.collider( scene.stars, scene.platforms );
	scene.physics.add.collider( scene.bombs, scene.platforms );

	scene.physics.add.collider( scene.player, scene.bombs, hitBomb, null, scene );
	scene.physics.add.overlap( scene.player, scene.stars, collectStar, null, scene );
}

function addKeyboardControls( scene ){
	scene.keys = scene.input.keyboard.createCursorKeys();
}

function movePlayerLeftRight( scene ){
	if( scene.keys.left.isDown ){
		scene.player.setVelocityX( -160 );
		scene.player.anims.play( "left", true );
	}
	else if( scene.keys.right.isDown ){
		scene.player.setVelocityX( 160 );
		scene.player.anims.play( "right", true );
	}
	else{
		scene.player.setVelocityX( 0 );
		scene.player.anims.play( "turn" );
	}
}

function jumpPlayer( scene ){
	if( scene.keys.up.isDown && scene.player.body.touching.down ){
		scene.player.setVelocityY( -330 );
	}
}

function movePlayer( scene ){
	movePlayerLeftRight( scene );
	jumpPlayer( scene );
}

function createStars( scene ){
	scene.stars = scene.physics.add.group( {
		"key": "star",
		"repeat": 11,
		"setXY": {
			"x": 12,
			"y": 0,
			"stepX": 70
		}
	} );

	scene.stars.children.iterate( ( child ) => {
		child.setBounceY( Phaser.Math.FloatBetween( 0.4, 0.8 ) );
	} );
}

function createBombs( scene ){
	scene.bombs = scene.physics.add.group();
}

function collectStar( player, star ){
	star.disableBody( true, true );

	this.score += 10;
	this.scoreText.setText( `Score: ${this.score}` );

	if( this.stars.countActive( true ) === 0 ){
		reenableStars( this );
		triggerBomb( this );
	}
}

function reenableStars( scene ){
	scene.stars.children.iterate( ( child ) => {
		child.enableBody( true, child.x, 0, true, true );
	} );
}

function triggerBomb( scene ){
	var x = ( scene.player.x < 400 ) ? Phaser.Math.Between( 400, 800 ) : Phaser.Math.Between( 0, 400 );

	var bomb = scene.bombs.create( x, 16, "bomb" );

	bomb.setBounce( 1 );
	bomb.setCollideWorldBounds( true );
	bomb.setVelocity( Phaser.Math.Between( -200, 200 ), 20 );
	bomb.allowGravity = false;
}

function hitBomb( player ){
	this.physics.pause();
	
	player.setTint( 0xff0000 );
	player.anims.play( "turn" );
}

function createUi( scene ){
	scene.scoreText = scene.add.text(
		16,
		16,
		"Score: 0",
		{
			"fontSize": "32px",
			"fill": "#000000"
		}
	);
}

main.init = function init(){
	this.platforms;
	this.player;
    
	this.stars;
	this.bombs;

	this.score = 0;
	this.scoreText;

	this.keys;
};

main.preload = function preload(){
	this.load.image( "sky", "assets/sky.png" );
	this.load.image( "ground", "assets/platform.png" );
	this.load.image( "star", "assets/star.png" );
	this.load.image( "bomb", "assets/bomb.png" );
	this.load.spritesheet(
		"dude",
		"assets/dude.png",
		{
			"frameWidth": 32,
			"frameHeight": 48
		}
	);
};

main.create = function create(){
	createMap( this );
	createAnimations( this );
	createPlayer( this );
    
	createStars( this );
	createBombs( this );
    
	addCollisions( this );
	addKeyboardControls( this );

	createUi( this );
};

main.update = function update(){
	movePlayer( this );
};

export { main };