var path = require( "path" );
var fs = require( "fs" );

var rollup = require( "rollup" );
var cjs = require( "rollup-plugin-commonjs" );
var resolve = require( "rollup-plugin-node-resolve" );

var copy = require( "recursive-copy" );
var mkdirp = require( "mkdirp" );
var globby = require( "globby" );

var projects = [
	"phaser-3-first-game",
	"super-milly-world",
	"pong"
];
var root = __dirname;
var output = path.join( root, "public" );
var copyOpts = {
	"overwrite": true,
	"filter": [
		"**/*",
		"!vendor/*"
	]
};

/* eslint-disable no-console */

function copyProjectIndex( name, cwd, out, next ){
	fs.copyFile(
		path.join( cwd, "index.html" ),
		path.join( out, "index.html" ),
		() => { /* swallow errors */ }
	);

	console.log( `Copied ${name} index file.` );
	next();
}

function bundleVendors( name, cwd, out, args ){
	var vendors = globby.sync( [ path.join( cwd, "source/vendor/*.js" ) ] );

	if( !args.includes( "--no-vendor" ) ){
		vendors.forEach( async ( vendorLoader ) => {
			let vendorName = path.basename( vendorLoader, ".js" );
			let vDest = path.join( out, "js/vendor", `${vendorName}.js` );
			let bundle = await rollup.rollup( {
				"input": vendorLoader,
				"plugins": [
					resolve(),
					cjs()
				]
			} );
			let { code } = await bundle.generate( {
				"format": "iife",
				"name": vendorName
			} );
		
			code += `\n\n export default ${vendorName};`;
		
			await fs.writeFile( vDest, code, "utf8", () => { /* swallow errors */ } );
			console.log( `Bundled vendor ${vendorName} for ${name}.` );
		} );
	}
}

function copyProjectSource( name, cwd, out, args, next ){
	bundleVendors( name, cwd, out, args );

	copy(
		path.join( cwd, "source" ),
		path.join( out, "js" ),
		copyOpts
	)
		.on( copy.events.COPY_FILE_COMPLETE, ( op ) => {
			console.log( `Copied ${op.dest}` );
		} )
		.on( copy.events.ERROR, ( e, op ) => {
			console.log( `Failed to copy ${op.dest}` );
		} )
		.then( () => {
			console.log( `Copied ${name} source files.` );
			next();
		} );
}

function copyProjectAssets( name, cwd, out, next ){
	copy(
		path.join( cwd, "assets" ),
		path.join( out, "assets" ),
		copyOpts
	)
		.on( copy.events.COPY_FILE_COMPLETE, ( op ) => {
			console.log( `Copied ${op.dest}` );
		} )
		.on( copy.events.ERROR, ( e, op ) => {
			console.log( `Failed to copy ${op.dest}` );
		} )
		.then( () => {
			console.log( `Copied ${name} game assets` );
			next();
		} );
}

projects.forEach( ( projectName ) => {
	let cwd = path.join( root, projectName );
	let out = path.join( output, projectName );
	let args = process.argv.slice( 2 );

	args = args.length > 0 ? args : [];

	mkdirp( out, ( e ) => {
		if( !e ){
			copyProjectIndex( projectName, cwd, out, () => {
				copyProjectSource( projectName, cwd, out, args, () => {
					copyProjectAssets( projectName, cwd, out, () => {} );
				} );
			} );
		}
	} );
} );