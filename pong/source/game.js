import Phaser from "./vendor/phaser.js";

import { main as MainScene } from "./scenes/main.js";

var config = {
	"type": Phaser.AUTO,
	"container": document.getElementById( "container" ),
	"width": 900,
	"height": 800,
	"scene": MainScene,
	"physics": {
		"default": "arcade",
		"arcade": {
			"gravity": false,
			"debug": false
		}
	}
};

new Phaser.Game( config ); // eslint-disable-line no-new