import Phaser from "../vendor/phaser.js";

var main = new Phaser.Scene( "main" );

var Field = {
	construct( scene ){
		var dash = 15;
		var gap = 10;
		var dashes = Math.ceil( scene.height / ( dash + gap ) );

		for( let i = 0; i < dashes; i++ ){
			let pt = dash + ( i * ( dash + gap ) );

			scene
				.add.image( ( scene.width / 2 ) - 1, pt, "pixel" )
				.setScale( 3, dash );
		}

		Field.createInvisibleFieldBoundaries( scene );
	},
	createInvisibleFieldBoundaries( scene ){
		var boundaryWidth = 64;

		scene.walls.left = scene.physics.add.image( 0, scene.height, "pixel" );
		scene.walls.right = scene.physics.add.image( scene.width + boundaryWidth, scene.height, "pixel" );
		scene.walls.top = scene.physics.add.image( scene.width, 0, "pixel" );
		scene.walls.bottom = scene.physics.add.image( scene.width, scene.height + boundaryWidth, "pixel" );

		scene.walls.left
			.setScale( boundaryWidth, scene.height )
			.setVisible( false )
			.setImmovable( true );
		scene.walls.right
			.setScale( boundaryWidth, scene.height )
			.setVisible( false )
			.setImmovable( true );
		scene.walls.top
			.setScale( scene.width, boundaryWidth )
			.setVisible( false )
			.setImmovable( true );
		scene.walls.bottom
			.setScale( scene.width, boundaryWidth )
			.setVisible( false )
			.setImmovable( true );
	}
};

var Paddle = {
	add( scene, side ){
		var pos = {
			"left": [ 11, scene.height - 150 ],
			"right": [ scene.width - 1, 150 ]
		};
		var paddle = scene.paddles[ side ] = scene.physics.add.image( pos[ side ][ 0 ], pos[ side ][ 1 ], "pixel" );

		paddle
			.setCollideWorldBounds( true )
			.setScale( 10, 50 )
			.setImmovable( true );
	},
	move( scene, side ){
		var keys = {
			"left": [ "w", "s" ],
			"right": [ "up", "down" ]
		};
		var paddle = scene.paddles[ side ];
		var keyPair = keys[ side ];

		if( scene.keys[ keyPair[ 0 ] ].isDown ){
			paddle.setVelocityY( -scene.paddleVelocity );
		}
		else if( scene.keys[ keyPair[ 1 ] ].isDown ){
			paddle.setVelocityY( scene.paddleVelocity );
		}
		else{
			paddle.setVelocityY( 0 );
		}
	}
};

var Ball = {
	reset( scene, lastStrike ){
		Ball.freeze( scene );
		Ball.replace( scene );
		setTimeout( () => {
			Ball.fly( scene, lastStrike );
		}, 5000 );
	},
	freeze( scene ){
		scene.ball
			.setVisible( false )
			.setVelocity( 0 );
	},
	replace( scene ){
		scene.ball.setPosition( scene.width / 2, Phaser.Math.Between( 150, scene.height - 150 ) );
	},
	inject( scene ){
		scene.ball = scene.physics.add.image( scene.width / 2, Phaser.Math.Between( 150, scene.height - 150 ), "pixel" );
		scene.ball
			.setScale( 5, 5 )
			.setBounce( scene.ballBounce )
			.setMaxVelocity( 1000, 1000 );

		Ball.freeze( scene );
	},
	fly( scene, lastWallStrike ){
		var walls = [ "left", "right" ];
		var ballVelocity;

		lastWallStrike = lastWallStrike || walls[ Math.floor( Math.random() * 2 ) ];

		if( lastWallStrike == "right" ){
			ballVelocity = scene.ballStartVelocity;
		}
		else{
			ballVelocity = -scene.ballStartVelocity;
		}

		scene.ball
			.setVisible( true )
			.setVelocity( ballVelocity, Phaser.Math.Between( -scene.ballStartVelocity, scene.ballStartVelocity ) );
	}
};

var Ui = {
	addScoreDisplays( scene ){
		var halfWidth = scene.width / 2;

		scene.scores.left.display = scene.add.text( halfWidth - 80, 5, scene.scores.left.score, {
			"fontSize": "80px",
			"fill": "#FFFFFF"
		} );
	
		scene.scores.right.display = scene.add.text( halfWidth + 20, 5, scene.scores.right.score, {
			"fontSize": "80px",
			"fill": "#FFFFFF"
		} );
	},
	listenToKeyboard( scene ){
		scene.keys = {
			"up": scene.input.keyboard.addKey( Phaser.Input.Keyboard.KeyCodes.UP ),
			"down": scene.input.keyboard.addKey( Phaser.Input.Keyboard.KeyCodes.DOWN ),
			"w": scene.input.keyboard.addKey( Phaser.Input.Keyboard.KeyCodes.W ),
			"s": scene.input.keyboard.addKey( Phaser.Input.Keyboard.KeyCodes.S )
		};
	},
	updateScores( scene ){
		scene.scores.left.display.setText( scene.scores.left.score );
		scene.scores.right.display.setText( scene.scores.right.score );

		if( scene.scores.left.score > 9 ){
			scene.scores.left.display.setPosition( ( scene.width / 2 ) - 130, 5 );
		}
	}
};

var Physics = {
	enableCollisions( scene ){
		scene.physics.add.collider( scene.ball, scene.paddles.left );
		scene.physics.add.collider( scene.ball, scene.paddles.right );

		scene.physics.add.collider( scene.ball, scene.walls.left, function strikeLeft(){
			Physics.wallStrike( this, "left" );
		}, null, scene );
		scene.physics.add.collider( scene.ball, scene.walls.right, function strikeRight(){
			Physics.wallStrike( this, "right" );
		}, null, scene );
		scene.physics.add.collider( scene.ball, scene.walls.top );
		scene.physics.add.collider( scene.ball, scene.walls.bottom );
	},
	wallStrike( scene, side ){
		var lastStrike = "";
		var handlers = {
			left(){
				scene.scores.right.score += 1;
				lastStrike = "left";
			},
			right(){
				scene.scores.left.score += 1;
				lastStrike = "right";
			}
		};

		handlers[ side ]();
		Ui.updateScores( scene );
		Ball.reset( scene, lastStrike );
	}
};

main.init = function init(){
	this.height = this.sys.game.config.height;
	this.width = this.sys.game.config.width;
	this.ballPause = 2500;
	this.ballBounce = 1.01; /* accelerates 1% every bounce */
	this.ballStartVelocity = 300;
	this.paddleVelocity = 800;
	this.scores = {
		"left": {
			"score": 0,
			"display": null
		},
		"right": {
			"score": 0,
			"display": null
		}
	};
	this.paddles = {
		"left": null,
		"right": null
	};
	this.walls = {
		"top": null,
		"right": null,
		"bottom": null,
		"left": null
	};
	this.ball = null;
};

main.preload = function preload(){
	this.load.image( "pixel", "assets/whitepixel.png" );
};

main.create = function create(){
	Field.construct( this );
	Ui.addScoreDisplays( this );
	
	Paddle.add( this, "left" );
	Paddle.add( this, "right" );
	
	Ui.listenToKeyboard( this );

	Ball.inject( this );

	Physics.enableCollisions( this );

	setTimeout( () => {
		Ball.fly( this );
	}, this.ballPause );
};

main.update = function update(){
	Paddle.move( this, "left" );
	Paddle.move( this, "right" );
};

export { main };